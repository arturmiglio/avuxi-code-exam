// modules import
import loadGoogleMapsAPI from 'load-google-maps-api'


// variables definition
let map
let gmapiOptions = {
	key: 'AIzaSyABop77lvCUdyGglbmVuU_4B0gGRuxnRr0',
}
let collChangeType
let currMapHeatType
let defMapHeatType = 'sights'
let btnLayers
let btnClose
let mapControls
let mapControlsToggle
const hoverEvents = ['mouseenter', 'touchend']
const houtEvents = ['click', 'touchend']

function initMap() {
	// define base map otions
	const mapOptions = {
		zoom: 10,
		center: new google.maps.LatLng(41.38605, 2.17014),
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		fullscreenControl: false
	}

	// attach the map to element
	map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions)

	// load avuxi script
	let sc = document.createElement('script')
	sc.id = 'vxscript'
	sc.type = 'text/javascript'
	sc.async = 'async'
	sc.src = 'http://m.avuxiapis.com/av/72a8d3b695629?callback=avapil&ln=en'
	document.body.appendChild(sc)
}

function avapil() {
	// start avuxi on map
	AVUXI.start(map)
	console.info(`MAP TYPE: ${AVUXI.hm.type}`)
}

// make avapil available on window when vxscript callsback
window.avapil = avapil

// get elements from DOM
function getElements() {
	// get collection of all type buttons
	collChangeType = document.querySelectorAll('.map_controls__heat_type')
	// get control btns
	btnLayers = document.querySelector('.icon-layers')
	btnClose = document.querySelector('.icon-close')
	mapControls = document.querySelector('.map_controls')
	mapControlsToggle = document.querySelector('.map_controls__toggle')
}

// init DOM event listeners
function initListeners() {
	// initiate each buttons listener
	for (let i = collChangeType.length - 1; i >= 0; i--) {
		collChangeType[i].addEventListener('click', onClickChangeType, true)
	}

	// hover/hout listeners for the controls box
	hoverEvents.forEach(e => mapControls.addEventListener(e, onHoverControls, false))
	houtEvents.forEach(e => btnClose.addEventListener(e, onHoutControls, false))
}

// change heat map type on click
function onClickChangeType(e) {
	// get map type from element data attribute
	let el = e.target
	let elType = el.dataset.heatMapType

	// clear all btns state
	for (let i = collChangeType.length - 1; i >= 0; i--) {
		collChangeType[i].setAttribute('data-active', 'false')
	}

	// if is 'none' btn, correct the parameter
	if (elType == 'none') {
		elType = ''
		currMapHeatType = ''
		changeMapType('')
		return
	}

	// set active state
	if (currMapHeatType !== elType) {
		el.setAttribute('data-active', el.getAttribute('data-active') === 'true' ? 'false' : 'true')
		currMapHeatType = elType
	} else {
		el.setAttribute('data-active', 'false')
		currMapHeatType = ''
	}

	changeMapType(elType)
}

function changeMapType(type) {
	// call avuxi heat map change function
	AVUXI.changeHM(type)
	console.info(`MAP TYPE: ${AVUXI.hm.type}`)
}

function onHoverControls() {
	mapControls.setAttribute('data-hover', '')
}

function onHoutControls() {
	mapControls.removeAttribute('data-hover')
}

// load gmaps then start everything else
loadGoogleMapsAPI(gmapiOptions).then(function(googleMaps) {
	initMap()
	getElements()
	initListeners()
}).catch((err) => {
	console.error(err)
})
