'use strict';

var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    config = require('../config.json'),
    argv = require('yargs').argv;

if (argv.lint) {

    gulp.task('watch', ['lint', 'browserify:vendor', 'browserify:app'], function() {
        gulp.watch(config.sprites + '/*-2x/*.png', ['resize-sprites']);
        gulp.watch(config.fonts + '/icomoon/**/*.*', ['icons']);
        gulp.watch(config.sass + '/**/*.scss', ['sass']);
        gulp.watch([
            config.js + '/**/*.{js,jsx}',
            '!' + config.js + '/libs/**/*.{js,jsx}',
            '!' + config.js + '/bundle.js',
            '!' + config.js + '/**/*.min.js'
        ], ['lint']);
        gulp.watch(config.img + '/**/*', ['copy']);
        gulp.watch(config.dev + 'index.html', ['copy']);
    });

} else {

    gulp.task('watch', ['browserify:vendor', 'browserify:app'], function() {
        gulp.watch(config.sprites + '/*-2x/*.png', ['resize-sprites']);
        gulp.watch(config.fonts + '/icomoon/**/*.*', ['icons']);
        gulp.watch(config.sass + '/**/*.scss', ['sass']);
        gulp.watch([
            config.js + '/**/*.{js,jsx}',
            '!' + config.js + '/libs/**/*.{js,jsx}',
            '!' + config.js + '/bundle.js',
            '!' + config.js + '/**/*.min.js'
        ]);
        gulp.watch(config.img + '/**/*', ['copy']);
        gulp.watch(config.dev + 'index.html', ['copy']);
    });
}