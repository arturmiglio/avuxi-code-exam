'use strict';

var gulp = require('gulp'),
    config = require('../config.json'),
    eslint = require('gulp-eslint'),
    handleErrors = require('../utils/handle-errors');

gulp.task('lint', function() {
    return gulp.src([
            config.js + '/**/*.{js,jsx}',
            '!' + config.js + '/libs/**/*.{js,jsx}',
            '!' + config.js + '/bundle.js',
            '!' + config.js + '/**/*.min.js'
        ])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
        .on('error', handleErrors);
});
