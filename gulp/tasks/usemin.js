'use strict';

var gulp = require('gulp'),
    config = require('../config.json'),
    usemin = require('gulp-usemin'),
    minifyCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    rev = require('gulp-rev');

gulp.task('usemin', function() {
    return gulp.src(global.dist + '/*.html')
        .pipe(usemin({
            css: [function () { return minifyCSS(), rev(); }],
            jsVendor: [function () { return uglify(); }],
            jsMain: [function () { return uglify(); }]
        }))
        .pipe(gulp.dest(global.dist));
});
