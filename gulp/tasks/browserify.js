'use strict';

var gulp = require('gulp'),
    config = require('../config.json'),
    _ = require('lodash'),
    gulpif = require('gulp-if'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    handleErrors = require('../utils/handle-errors'),
    bundleLogger = require('../utils/bundle-logger'),
    bra = require('browserify-require-async'),
    wrap = require('gulp-wrap'),
    argv = require('yargs').argv,
    sourcemaps = require('gulp-sourcemaps');

var source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    babelify = require('babelify');


var packageJson = require('../../package.json');
var dependencies = Object.keys(packageJson && packageJson.dependencies || {});

/* ###########################
   ###### VENDOR BUNDLE ######
   ########################### */

gulp.task('browserify:vendor', function () {

    var reportFinished = function() {
        bundleLogger.end('vendor.js');
    };

    var bundler = browserify({
        extensions: ['.js', '.jsx'],
        cache: {},
        packageCache: {},
        debug: true
    }).transform(bra);

    var bundle = function() {

        bundleLogger.start('vendor.js');

        return bundler
            .require(dependencies)
            .bundle()
            .on('error', handleErrors)
            .pipe(source('vendor.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            // .pipe(gulpif(!global.isWatching, uglify()))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(global.dist + '/' + config.js))
            .pipe(gulpif(global.isWatching, browserSync.reload({stream: true})))
            .on('end', reportFinished)
    };

    if (global.isWatching) {
        bundler = watchify(bundler);
        bundler.on('update', bundle);
        bundleLogger.watch('vendor.js');
    }

    return bundle();
});

/* ######################## 
   ###### APP BUNDLE ######
   ######################## */

gulp.task('browserify:app', function () {

    var reportFinished = function() {
        bundleLogger.end('bundle.js');
    };

    var bundler = browserify({ 
        entries: [config.dev + config.js + '/app.js'],
        extensions: ['.js', '.jsx'],
        cache: {},
        packageCache: {},
        debug: true
    }).transform(babelify); 

    var bundle = function() {

        bundleLogger.start('bundle.js');

        return bundler
            .external(dependencies)
            .bundle()
            .on('error', handleErrors)
            .pipe(source('bundle.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            // .pipe(gulpif(!global.isWatching, uglify()))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(global.dist + '/' + config.js))
            .pipe(gulpif(global.isWatching, browserSync.reload({stream: true})))
            .on('end', reportFinished)
    };

    if (global.isWatching) {
        bundler = watchify(bundler);
        bundler.on('update', bundle);
        bundleLogger.watch('bundle.js');
    }

    return bundle();
});
