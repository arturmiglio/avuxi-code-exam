'use strict';

var gulp = require('gulp'),
    runSequence = require('run-sequence'),
    argv = require('yargs').argv,
    config = require('../config.json');

var sequence = [
    [
        'clean'
    ],
    [
        'icons'
    ],
    [
        'sass', 
        'browserify:vendor', 
        'browserify:app'
    ],
    'usemin',
    'copy'
];

gulp.task('build', function(cb) {
    global.dist = config.build;
    
    if(argv.deploy) {
        sequence.push('deploy');
    }
    sequence.push(cb);

    return runSequence.apply(this, sequence);
});
