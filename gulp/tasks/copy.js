'use strict';

var gulp = require('gulp'),
	browserSync = require('browser-sync'),
    config = require('../config.json');

gulp.task('copy', function() {
    return gulp.src([
    		'index.html',
            config.img + '/**',
            config.fonts + '/**',
            '.htaccess',
            '!' + config.img + '/' + config.sprites,
        ], {base: '.'})
        .pipe(gulp.dest(global.dist))
        .pipe(browserSync.stream());
});