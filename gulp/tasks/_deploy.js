'use strict';

var gulp = require('gulp'),
    shell = require('gulp-shell'),
    gutil = require('gulp-util'),
    ftp = require('vinyl-ftp'),
    config = require('../config.json');

gulp.task('deploy', shell.task([
  'firebase deploy',
  'firebase open hosting:site'
]));

