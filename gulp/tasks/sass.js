'use strict';

var gulp = require('gulp'),
    config = require('../config.json'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    gulpif = require('gulp-if'),
    autoprefixer = require('autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    handleErrors = require('../utils/handle-errors'),
    filter = require('gulp-filter'),
    plumber = require('gulp-plumber'),
    minifyCSS = require('gulp-clean-css');

gulp.task('sass', function() {

    var plugins = [
        autoprefixer({browsers: ['last 2 versions','ie >= 9','iOS 8']})
    ];

    return gulp.src(config.sass + '/**/*.scss')
        .pipe(gulpif(global.isWatching, sourcemaps.init()))
        .pipe(sass({
            includePaths: [
                'node_modules/'
            ]
        }).on('error', sass.logError))
        .pipe(postcss(plugins))
        .pipe(gulpif(global.isWatching, sourcemaps.write('./')))
        .pipe(plumber({ errorHandler: onError }))
        .pipe(gulp.dest(global.dist + '/' + config.css))
        .pipe(filter(['**/*.css']))
        .pipe(browserSync.stream());
});

function onError(err) {
    var message = new gutil.PluginError(err.plugin, err.message).toString();
    process.stderr.write(message + '\n');
    gutil.beep();
}