'use strict';

var gulp = require('gulp'),
    runSequence = require('run-sequence'),
    config = require('../config.json');

gulp.task('default', function(cb) {
    global.isWatching = true;
    global.dist = config.dist;

    runSequence(
        [
            'icons',
        ],
        [
            'sass',
        ],
        [   
            'copy',
        ],
        'watch',
        'serve-dev',
        cb);
});
